import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;

import play.*;
import play.db.jpa.Blob;
import play.jobs.*;
import play.libs.MimeTypes;
import play.test.*;
import models.*;
 
@OnApplicationStart
public class Bootstrap extends Job 
{ 
  public void doJob() throws FileNotFoundException
  {
	    //Fixtures.deleteDatabase();
	    Fixtures.loadModels("data.yml");
	    List<Candidate> candidates = Candidate.findAll();
	    for (Candidate candidate : candidates)
	    {
	        Blob blob = new Blob();
	        blob.set(new FileInputStream("public/images/"+candidate.imagefile), MimeTypes.getContentType("public/images/"+candidate.imagefile));
	        candidate.profilePicture = blob;
	        candidate.save();      
	    }
  }
}