package controllers;

import play.*;
import play.mvc.*;

import java.util.*;

import models.*;

public class Accounts extends Controller
{
  public static void signup()
  {
	List<Candidate> candidates = Candidate.findAll();
    render(candidates);
  }
  
  public static void signupError()
  {
	List<Candidate> candidates = Candidate.findAll();
    render(candidates);
  }


  public static void login()
  {
    render();
  }

  public static void logout()
  {
    session.clear();
    index();
  }

  public static void index()
  {
    render();
  }
  
  public static User currentlyLoggedIn()
  {
    User user = null;
    if (session.contains("logged_in_userid"))
    {
      String userId = session.get("logged_in_userid");
      user = User.findById(Long.parseLong(userId));
    }
    else
    {
      index();
    }
    return user;
  }
  
  public static void register(String firstName, String lastName, String email, int age, String password, boolean usaCitizen, String addressLine1, String addressLine2, String country, String postcode, String townCity, Long id)
  {
    List<User> users = User.findAll();
    User user = new User(firstName, lastName, email, age, password, usaCitizen, addressLine1, addressLine2, country, postcode, townCity);
	  for (User usercheck:users)
	    {
	      if (usercheck.email.contains(email))
	      {
	    	  Logger.info("Attempting to authenticate with " + email + " ..... --->error," + " already exists " + " containing email " + user.email);
	    	  signupError();
	      }
	      else
	      {
	    Logger.info(firstName + " " + lastName + " " + email + " " + password + " " + age + " " + addressLine1 + " " + addressLine2 + " " + country + " " + postcode + " " + townCity);
	      user.save();
	      Candidate candidate = Candidate.findById(id);
	      user.befriend(candidate);
	    	  Logger.info( email + " " + "does not exist in database containin" + " " + user.email);
	          login();
	      }
	    }
	  }
  

  
  public static void authenticate(String email, String password)
  {
	  Logger.info("Attempting to authenticate with " + email + ":" +  password);

	  User user = User.findByEmail(email);
		  if ((user != null) && (user.checkPassword(password) == true))
		  {
			  Logger.info("Authentication successful");
			  session.put("logged_in_userid", user.id);
			  Welcome.index();

		  }
		  else
		  {
			  Logger.info("Authentication failed");
			  login();  
		  }
	  } 
  }
