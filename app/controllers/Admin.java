package controllers;

import play.*;
import play.mvc.*;

import java.util.*;

import models.*;

public class Admin extends Controller
{


	public static void AddCandidate()
	{
		User user = Accounts.currentlyLoggedIn();
		if (user == null)
		{
			Logger.info("You are Not Logged in ..... Redirecting");
			Accounts.signup();
		}
		else
		{
			if(user.checkAdmin(user.email, user.password) == true)
			{
				List<Candidate> candidates = Candidate.findAll();
				List<Office> office = Office.findAll();
				render(candidates, office);
			}
			else
			{
				Logger.info("You are Not Logged in ..... Redirecting");
				Accounts.signup();
			}
		}
	}


	public static void register(String candfirstName, String candlastName, String email, String phoneNo, String candbio, String officeAddress, String postcode, String townCity, String note, String country, int donationRange,String officeName, double lng, double lat, Long id)
	{
		Logger.info(candfirstName + " " + candlastName + " " + email + " " + phoneNo + " " + candbio + " " + officeAddress + " " + postcode + " " + townCity + " " + note + " " + country);

		Candidate candidate = new Candidate(candfirstName, candlastName, email, phoneNo, candbio, officeAddress, postcode, townCity, note, country, donationRange, lng, lat);
		candidate.save();
		candidate.addToOffice(officeName, candidate);
		AddCandidate();
	}

	public static void Report()
	{
		User user = Accounts.currentlyLoggedIn();
		if (user == null)
		{
			Logger.info("You are Not Logged in ..... Redirecting");
			Accounts.signup();
		}
		else
		{
			if(user.checkAdmin(user.email, user.password) == true)
			{
				String adminCheck = "Admin";
				List<Office> office = Office.findAll();
				List<Candidate> candidates = Candidate.findAll();
				List<User> users = User.findAll();
				ArrayList<User> AdminlessResults = new ArrayList<User>();
				for(User removedAdmin : users) 
					if ( !removedAdmin.email.contains(adminCheck) )
					{
						AdminlessResults.add(removedAdmin);
					}
				render(AdminlessResults, candidates, office);
			}
		}
	}

	public static void EditCandidate(Long id)
	{
		User user = Accounts.currentlyLoggedIn();
		if (user == null)
		{
			Logger.info("You are Not Logged in ..... Redirecting");
			Accounts.signup();
		}
		else
		{
			if(user.checkAdmin(user.email, user.password) == true)
			{
				List<Office> office = Office.findAll();
				Candidate candidate =  Candidate.findById(id);
				List<Candidate> candidates = Candidate.findAll();
				List<User> users = User.findAll();
				render(users, candidates, candidate, office);
			}
		}
	}

	public static void changeDetails(String candfirstName, String candlastName, String email, String phoneNo, String candbio, String officeAddress, String postcode, String townCity, String note, String country, int donationRange,String officeName,double lng, double lat, Long id)
	{
		Candidate candidate =  Candidate.findById(id);
		candidate.candfirstName = candfirstName;
		candidate.candlastName  = candlastName;
		candidate.email     = email;
		candidate.phoneNo = phoneNo;
		candidate.candbio  = candbio;
		candidate.officeAddress = officeAddress;
		candidate.postcode = postcode;
		candidate.townCity = townCity;
		candidate.note = note;
		candidate.country = country;
		candidate.percentage="0";
		candidate.donationRange = donationRange;
		candidate.lng=lng;
		candidate.lat=lat;
	

		Logger.info(candfirstName + " " + candlastName + " " + email + " " + phoneNo + " " + candbio + " " + officeAddress + " " + postcode + " " + townCity + " " + note + " " + country + " " +  donationRange);
		candidate.save();
		Candidate candidate1 = Candidate.findById(id);
		candidate.addToOffice(officeName, candidate1);
		EditCandidate(id);
	}

}