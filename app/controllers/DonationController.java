package controllers;

import play.*;
import play.mvc.*;

import java.util.*;

import javax.persistence.Entity;
import javax.persistence.OneToMany;

import models.*;
public class DonationController extends Controller 
{


	public static void index()
	{
		User user = Accounts.currentlyLoggedIn();
		if (user == null)
		{
			Logger.info("You are Not Logged in ..... Redirecting");
			Accounts.signup();
		}
		else
		{
			if(user.checkAdmin(user.email, user.password) == true)
			{
				Logger.info("You are Admin ..... Redirecting");
				Admin.AddCandidate();
			}
			else
			{
				List<Candidate> candidates = Candidate.findAll();
				/*for (Candidate candidate:candidates) {
					getPercentTargetAchieved(candidate);
				}*/
				Logger.info("Donation ctrler : user is " + user.email);
				render(user, candidates);
			}
		}
	}
	
	public static void exceed()
	{
		User user = Accounts.currentlyLoggedIn();
		if (user == null)
		{
			Logger.info("You are Not Logged in ..... Redirecting");
			Accounts.signup();
		}
		else
		{
			if(user.checkAdmin(user.email, user.password) == true)
			{
				Logger.info("You are Admin ..... Redirecting");
				Admin.AddCandidate();
			}
			else
			{
				List<Candidate> candidates = Candidate.findAll();
				/*for (Candidate candidate:candidates) {
					getPercentTargetAchieved(candidate);
				}*/
				Logger.info("Donation ctrler : user is " + user.email);
				render(user, candidates);
			}
		}
	}

	public static void donate(long amountDonated, String methodDonated, Long id)
	{
		int total = DonationController.checkDonationTotal(id);
		Candidate toCandidate = Candidate.findById(id);
		Logger.info("amount donated " + amountDonated + " " + "method donated " + methodDonated + " " + "Who to support " + toCandidate.candfirstName + " " + toCandidate.candlastName);
        
		User user = Accounts.currentlyLoggedIn();
		if (user == null)
		{
			Logger.info("Donation class : Unable to getCurrentuser");
			Accounts.login();
		}
		else
		{
			Logger.info("Donation range: "+toCandidate.donationRange+" total: "+total+" diff: "+(toCandidate.donationRange-total));
			if (toCandidate.donationRange-total==0)
			{
				Logger.info("Donation class : Unable to donate due that Donation target has been Achieved");
				Profile.error(id);
			}
			else
			{
				//
				if (amountDonated > (toCandidate.donationRange-total))
				{
					Logger.info("Donation class : Unable to donate due to amount donated too large, please chose a lesser amount");
					DonationController.exceed();
				}
				else
				{
					User.addDonation(user, amountDonated, methodDonated, toCandidate);
				}
			}
		}
		Profile.Thanks(id);
		}



	static int checkDonationTotal(long id)
	{
		Candidate candidate = Candidate.findById(id);
		List<Donation> allDonations = Donation.findAll();
		int total = 0;
		for (Donation donation : allDonations)
		{
			if (donation.candidate == candidate) {
				total += donation.amountDonated;
			}
		}
		return total;
	}

	

/*	public static String getPercentTargetAchieved(Candidate to)
	{
		List<Donation> allDonations = Donation.findAll();
		long total = 0;
		for (Donation donation : allDonations)
		{
			if (donation.candidate == to) {
				total += donation.amountDonated;
			}
		}
		long target = getDonationTarget();
		long percentachieved = (total * 100 / target);
		String progress = String.valueOf(percentachieved);
		to.percentage=progress;
		to.save();        Logger.info("Percent of target achieved (string) " + progress
				+ " percentachieved (long)= " + percentachieved);
		return progress;
	}*/


	public static void renderReport()
	{
		User user = Accounts.currentlyLoggedIn();
		if (user == null)
		{
			Logger.info("You are Not Logged in ..... Redirecting");
			Accounts.signup();
		}
		else
		{
			if(user.checkAdmin(user.email, user.password) == true)
			{
				Logger.info("You are Admin ..... Redirecting");
				Admin.AddCandidate();
			}
			else
			{
				List<Donation> donations = Donation.findAll();
				render(donations, user);
			}
		}
	}
}
