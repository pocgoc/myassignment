package controllers;

import play.*;
import play.mvc.*;

import java.util.*;

import models.*;

public class EditProfile extends Controller
{
	public static void change(String firstName, String lastName, String email, int age, String password, String addressLine1, String addressLine2,String country, String postcode, String townCity, Long id)
	{
		User user = Accounts.currentlyLoggedIn();
		user.firstName = firstName;
		user.lastName  = lastName;
		user.email     = email;
		user.password  = password;
		user.age = age;
		user.addressLine1 = addressLine1;
		user.addressLine2 = addressLine2;
		user.country = country;
		user.postcode = postcode;
		user.townCity = townCity;

		Logger.info(firstName + " " + lastName + " " + email + " " + password + " " + age + " " + addressLine1 + " " + addressLine2 + " " + country + " " + postcode + " " + townCity);
		user.save();
		Candidate candidate = Candidate.findById(id);
		user.befriend(candidate);
		index();
	}

	public static void index()
	{
		User user = Accounts.currentlyLoggedIn();
		if (user == null)
		{
			Logger.info("You are Not Logged in ..... Redirecting");
			Accounts.signup();
		}
		else
		{
			if(user.checkAdmin(user.email, user.password) == true)
			{
				Logger.info("You are Admin ..... Redirecting");
				Admin.AddCandidate();
			}
			else
			{
				List<Candidate> candidates = Candidate.findAll();
				render(user, candidates);
			}
		}
	}
}