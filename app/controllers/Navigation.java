package controllers;

import play.*;
import play.mvc.*;

import java.util.*;

import models.*;

public class Navigation extends Controller
{
  public static void index()
  {
    User user = Accounts.currentlyLoggedIn();
    render(user);
  }
}