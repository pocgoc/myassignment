package controllers;

import play.*;
import play.mvc.*;

import java.util.*;

import models.*;
import play.db.jpa.Blob;

public class Profile extends Controller
{

	public static void getPicture(Long id) 
	{
		Candidate candidate =  Candidate.findById(id);
		Blob picture = candidate.profilePicture;
		if (picture.exists())
		{
			response.setContentTypeIfNotSet(picture.type());
			renderBinary(picture.get());
		}
	}

	public static void uploadPicture(Long id, Blob picture)
	{
		Candidate candidate =  Candidate.findById(id);
		candidate.profilePicture = picture;
		candidate.save();
		visit(id);
	}


	public static void visit(Long id)
	{
		User user = Accounts.currentlyLoggedIn();
		if (user == null)
		{
			Logger.info("You are Not Logged in ..... Redirecting");
			Accounts.signup();
		}
		else
		{
			if(user.checkAdmin(user.email, user.password) == true)
			{
				Logger.info("You are Admin ..... Redirecting");
				Admin.AddCandidate();
			}
			else
			{
				Candidate candidate = Candidate.findById(id);
				getPercentTargetAchieved(candidate);
				int total = DonationController.checkDonationTotal(id);
				Logger.info("Donation ctrler : user is " + candidate.candfirstName);
				render(user,candidate, candidate.inbox, total);
			}
		}

	}
	
	public static void error(Long id)
	{
		Logger.info("Showing error");
		User user = Accounts.currentlyLoggedIn();
		if (user == null)
		{
			Logger.info("You are Not Logged in ..... Redirecting");
			Accounts.signup();
		}
		else
		{
			if(user.checkAdmin(user.email, user.password) == true)
			{
				Logger.info("You are Admin ..... Redirecting");
				Admin.AddCandidate();
			}
			else
			{
				Candidate candidate = Candidate.findById(id);
				getPercentTargetAchieved(candidate);
				int total = DonationController.checkDonationTotal(id);
				Logger.info("Donation ctrler : user is " + candidate.candfirstName);
				render(user,candidate, candidate.inbox, total);
			}
		}

	}
	
	public static void Thanks(Long id)
	{
		Logger.info("Showing error");
		User user = Accounts.currentlyLoggedIn();
		if (user == null)
		{
			Logger.info("You are Not Logged in ..... Redirecting");
			Accounts.signup();
		}
		else
		{
			if(user.checkAdmin(user.email, user.password) == true)
			{
				Logger.info("You are Admin ..... Redirecting");
				Admin.AddCandidate();
			}
			else
			{
				Candidate candidate = Candidate.findById(id);
				getPercentTargetAchieved(candidate);
				int total = DonationController.checkDonationTotal(id);
				Logger.info("Donation ctrler : user is " + candidate.candfirstName);
				render(user,candidate, candidate.inbox, total);
			}
		}

	}

	public static void sendMessage(Long id, String messageText)
	{
		User user = Accounts.currentlyLoggedIn();
		if (user == null)
		{
			Logger.info("You are Not Logged in ..... Redirecting");
			Accounts.signup();
		}
		else
		{
			if(user.checkAdmin(user.email, user.password) == true)
			{
				Logger.info("You are Admin ..... Redirecting");
				Admin.AddCandidate();
			}
			else
			{
				String userId = session.get("logged_in_userid");
				User fromUser = User.findById(Long.parseLong(userId));
				Candidate toUser = Candidate.findById(id);

				Logger.info("Message from user " + 
						fromUser.firstName + ' ' + fromUser.lastName +" to " +
						toUser.candfirstName + ' ' + toUser.candlastName +": " +
						messageText);    

				fromUser.sendtoCandidate(toUser, messageText);
				visit(id);
			}
		}
	} 
	public static void follow(Long id)
	{
		User user = Accounts.currentlyLoggedIn();
		if (user == null)
		{
			Logger.info("You are Not Logged in ..... Redirecting");
			Accounts.signup();
		}
		else
		{
			if(user.checkAdmin(user.email, user.password) == true)
			{
				Logger.info("You are Admin ..... Redirecting");
				Admin.AddCandidate();
			}
			else
			{
				Candidate candidate = Candidate.findById(id);

				String userId = session.get("logged_in_userid");
				User me = User.findById(Long.parseLong(userId));
				Logger.info("following: " + candidate.candfirstName );


				me.befriend(candidate);
				Welcome.index();
			}
		}
	}

	public static void drop(Long id)
	{
		User user = Accounts.currentlyLoggedIn();
		if (user == null)
		{
			Logger.info("You are Not Logged in ..... Redirecting");
			Accounts.signup();
		}
		else
		{
			if(user.checkAdmin(user.email, user.password) == true)
			{
				Logger.info("You are Admin ..... Redirecting");
				Admin.AddCandidate();
			}
			else
			{
				Candidate candidate = Candidate.findById(id);  
				String userId = session.get("logged_in_userid");
				User me = User.findById(Long.parseLong(userId));

				me.unfriend(candidate);
				Logger.info("Dropping " + candidate.email);
				Welcome.index();
			}
		}
	} 

	public static String getPercentTargetAchieved(Candidate candidate)
	{
		List<Donation> allDonations = Donation.findAll();
		long total = 0;
		for (Donation donation : allDonations)
		{
			if (donation.candidate == candidate) {
				total += donation.amountDonated;
			}
		}
		long percentachieved = (total * 100 / candidate.donationRange);
		String progress = String.valueOf(percentachieved);
		candidate.percentage=progress;
		candidate.save();        
		Logger.info("Percent of target achieved (string) " + progress + " percentachieved (long)= " + percentachieved);
		return progress;
	}
}