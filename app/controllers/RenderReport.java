package controllers;


import play.*;
import play.mvc.*;

import java.util.*;

import models.*;



public class RenderReport extends Controller
{
	public static void index()
	{
		User user = Accounts.currentlyLoggedIn();
		if (user == null)
		{
			Logger.info("You are Not Logged in ..... Redirecting");
			Accounts.signup();
		}
		else
		{
			if(user.checkAdmin(user.email, user.password) == true)
			{
				Logger.info("You are Admin ..... Redirecting");
				Admin.AddCandidate();
			}
			else
			{
				render(user);
			}
		}
	}

	public static void getFilteredAge(int ageRangeA, int ageRangeB)
	{
		User user = Accounts.currentlyLoggedIn();
		if (user == null)
		{
			Logger.info("You are Not Logged in ..... Redirecting");
			Accounts.signup();
		}
		else
		{
			if(user.checkAdmin(user.email, user.password) == true)
			{
				Logger.info("You are Admin ..... Redirecting");
				Admin.AddCandidate();
			}
			else
			{
				List<Donation> donation = Donation.findAll();
				ArrayList<Donation> ageResults = new ArrayList<Donation>();
				for(Donation filteredAgeResults : donation) 
					if (filteredAgeResults.from.age >= ageRangeA && filteredAgeResults.from.age <= ageRangeB )
					{
						ageResults.add(filteredAgeResults);
					}
				render(ageResults, user);
			}
		}
	}

	public static void getFilteredCountry(String searchedCountry)
	{
		User user = Accounts.currentlyLoggedIn();
		if (user == null)
		{
			Logger.info("You are Not Logged in ..... Redirecting");
			Accounts.signup();
		}
		else
		{
			if(user.checkAdmin(user.email, user.password) == true)
			{
				Logger.info("You are Admin ..... Redirecting");
				Admin.AddCandidate();
			}
			else
			{
				List<Donation> donation = Donation.findAll();
				ArrayList<Donation> countryResults = new ArrayList<Donation>();
				for(Donation filteredCountryResults : donation) 
					if (filteredCountryResults.from.country.contains(searchedCountry) )
					{
						countryResults.add(filteredCountryResults);
					}
				render(countryResults, user);
			}
		}
	}
}
