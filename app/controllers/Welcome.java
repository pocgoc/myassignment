package controllers;

import java.util.List;

import play.*;
import play.mvc.*;
import models.*;

public class Welcome extends Controller {

	public static void index() 
	{	  
		User user = Accounts.currentlyLoggedIn();
		List<Candidate> candidates = Candidate.findAll();
		if (user == null)
		{
			Logger.info("You are Not Logged in ..... Redirecting");
			Accounts.signup();
		}
		else
		{
			if(user.checkAdmin(user.email, user.password) == true)
			{
				Logger.info("You are Admin ..... Redirecting");
				Admin.AddCandidate();
			}
			else
			{
				Logger.info("Choose who to support, as many party memebers as you like");
				render(user, candidates);
			}
		}
	}

}
