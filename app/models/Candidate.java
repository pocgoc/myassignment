package models;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import controllers.Admin;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import play.Logger;
import play.db.jpa.Model;
import play.db.jpa.Blob;


@Entity
public class Candidate extends Model
{
	public String candfirstName;
	public String candlastName;
	public String email;
	public String phoneNo;
	public String candbio;
	public String officeAddress;
	public String country;
	public String postcode;
	public String townCity;
	public String note;
	public Blob profilePicture;
	public String percentage;
	public int donationRange;
	public String imagefile;
	public double lat;
	public double lng;
	
	@OneToMany(mappedBy = "to")
	public List<Message> inbox = new ArrayList<Message>();
	
	@OneToMany(mappedBy = "targetCandidate")
	public static List<Office> offices = new ArrayList<Office>();
	
	
   public Candidate(String candfirstName, String candlastName, String email, String phoneNo, String candbio, String officeAddress, String postcode, String townCity, String note, String country, int donationRange, double lng, double lat)
	{
		this.candfirstName = candfirstName;
		this.candlastName  = candlastName;
		this.email     = email;
		this.phoneNo = phoneNo;
		this.candbio  = candbio;
		this.officeAddress = officeAddress;
		this.country = country;
		this.postcode = postcode;
		this.townCity = townCity;
		this.note = note;
		this.percentage="0";
		this.donationRange=donationRange;
		this.lng=lng;
		this.lat=lat;
		
		
	}
   
   public void addToOffice(String officeName, Candidate targetCandidate)
	  {
		Office thisOffice = new Office(officeName, targetCandidate);
	    
	    for (Office selectOffice:Candidate.offices)
	    {
	      if (selectOffice.targetCandidate==targetCandidate)
	      {
	    	  Admin.AddCandidate();
	      }
	    }
	    Candidate.offices.add(thisOffice);
	    thisOffice.save();
	    save();
	  }

	  public void dropOffice(Candidate targetCandidate)
	  {
		  Office thisOffice = null;
	    
		  for (Office selectOffice:Candidate.offices)
		    {
		      if (selectOffice.targetCandidate==targetCandidate)
		      {
		    	  thisOffice = selectOffice;
	      }
	    }
		  Candidate.offices.remove(thisOffice);
		  thisOffice.delete();
	    save();
	  }
	  
}