package models;

import java.util.Date;

import javax.persistence.*;

import play.db.jpa.*;

@Entity
public class Message extends Model
{
  public String messageText;

  @ManyToOne
  public User sender;

  @ManyToOne
  public Candidate to;

  public Date postedAt;

  public Message(User sender, Candidate to, String messageText)
  {
    this.sender = sender;
    this.to = to;
    this.messageText = messageText;
  }
}