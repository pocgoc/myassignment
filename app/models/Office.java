package models;

import java.util.ArrayList;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import controllers.Admin;
import controllers.Welcome;
import play.db.jpa.Model;

@Entity
public class Office extends Model
{

	@OneToOne()
    public Candidate targetCandidate;
	
	public String officeName;
	
	public Office(String officeName, Candidate targetCandidate)
	{
		this.officeName = officeName;
		this.targetCandidate = targetCandidate;
	}

}
