package models;

import javax.persistence.*;
import play.db.jpa.Model;

@Entity
public class Support extends Model
{
  @ManyToOne()
  public User sourceUser;

  @ManyToOne()
  public Candidate targetUser;

  public Support(User source, Candidate target)
  {
    sourceUser = source;
    targetUser = target;
  }
}