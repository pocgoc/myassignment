package models;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import controllers.Accounts;
import controllers.Welcome;

import java.util.ArrayList;
import java.util.List;

import play.Logger;
import play.db.jpa.Model;

@Entity
public class User extends Model
{
	public String firstName;
	public String lastName;
	public String email;
	public String password;
	public boolean usaCitizen;
	public int age;
	public String addressLine1;
	public String addressLine2;
	public String country;
	public String postcode;
	public String townCity;


	@OneToMany(mappedBy = "to")
	public List<Message> inbox = new ArrayList<Message>();

    @OneToMany(mappedBy = "sender")
	public List<Message> outbox = new ArrayList<Message>();
	
	@OneToMany(mappedBy = "sourceUser")
    public List<Support> supporters = new ArrayList<Support>();
	
   @OneToMany(mappedBy = "from", cascade = CascadeType.ALL)
	public static List<Donation> donations = new ArrayList<Donation>();
	
	public User(String firstName, String lastName, String email, int age, String password, boolean usaCitizen, String addressLine1, String addressLine2,String country, String postcode, String townCity)
	{
		this.firstName = firstName;
		this.lastName  = lastName;
		this.email     = email;
		this.age = age;
		this.password  = password;
		this.usaCitizen = usaCitizen;
		this.addressLine1 = addressLine1;
		this.addressLine2 = addressLine2;
		this.country = country;
		this.postcode = postcode;
		this.townCity = townCity;
	}
	
	public static User findByEmail(String email)
	{
		return find("email", email).first();
	}

	public boolean checkPassword(String password)
	{
		return this.password.equals(password);
	}
	
	public static void addDonation(User from, long amountDonated, String methodDonated, Candidate candidate)
	{
	Donation bal = new Donation(from, methodDonated, amountDonated, candidate);
	donations.add(bal);
	bal.save();
	}
	
	public void befriend(Candidate friend)
	  {
		Support thisSupporting = new Support(this, friend);
	    
	    for (Support supporting:supporters)
	    {
	      if (supporting.targetUser== friend)
	      {
	    	  Welcome.index();
	      }
	    }
	    supporters.add(thisSupporting);
	    thisSupporting.save();
	    save();
	  }

	  public void unfriend(Candidate friend)
	  {
		  Support thisSupport = null;
	    
	    for (Support supporting:supporters)
	    {
	      if (supporting.targetUser== friend)
	      {
	    	  thisSupport = supporting;
	      }
	    }
	    supporters.remove(thisSupport);
	    thisSupport.delete();
	    save();
	  }
	  
	  public void sendtoCandidate (Candidate to, String messageText)
	  {
	    Message message = new Message (this, to, messageText);
	    outbox.add(message);
	    to.inbox.add(message);
	    message.save();
	  }
	  
	  public boolean checkAdmin(String email, String password)
		{
			return (this.email.equals("Admin") && this.password.equals("secret"));
			
		}
	  
	  public boolean checkExistingUser(String email)
		{
		List<User> users = User.findAll();
		  for (User usercheck:users)
		    {
		      if (usercheck.email==email)
		      {
		    	  return true;
		      }
		    }
		return false;
		}
	
}