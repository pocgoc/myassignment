function initialize() {
    var map;
    var marker;
    var inputLongit = $('#lng').val();
    var inputLatit = $('#lat').val();
    var latlng = new google.maps.LatLng((inputLatit), (inputLongit));

    var mapOptions = {
        zoom : 8,
        center : new google.maps.LatLng((inputLatit), (inputLongit)),
        mapTypeId : google.maps.MapTypeId.ROADMAP
    };
    var mapDiv = document.getElementById('map_canvas');
    map = new google.maps.Map(mapDiv,mapOptions);
    mapDiv.style.width = '730px';
    mapDiv.style.height = '600px';
    // place a marker
        marker = new google.maps.Marker({
        map : map,
        draggable : true,
        position : latlng,
        title : "Location of Candidate!"
    });
    // To add the marker to the map, call setMap();
    marker.setMap(map); 
    //marker listener populates hidden fields ondragend
    google.maps.event.addListener(marker, 'dragend', function() {
    	var latLng = marker.getPosition();
        var latitude = latLng.lat();
        var longitude = latLng.lng();
        //publish lat long in geolocation control in html page
        $("#long").val(longitude);
        $("#latit").val(latitude);
        //update the new marker position
        map.setCenter(latLng);
      });
}
google.maps.event.addDomListener(window, 'load', initialize);
