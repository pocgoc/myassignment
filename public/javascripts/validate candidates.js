function validateCandidate(input) {
	if (document.getElementById("candfirstName").value == "") {
		alert('Please Enter First Name');
		document.getElementById('candfirstName').style.borderColor = "red";
		return false;
	}
	else
	{
		document.getElementById('candfirstName').style.borderColor = "green";
		if (document.getElementById("candlastName").value == "") {
			alert('Please Enter Last Name');
			document.getElementById('candlastName').style.borderColor = "red";
			return false;
		}
		else
		{
			document.getElementById('candlastName').style.borderColor = "green";
			if (document.getElementById("email").value == "") {
				alert('Please Enter email');
				document.getElementById('email').style.borderColor = "red";
				return false;
			}
			else
			{
				document.getElementById('email').style.borderColor = "green";
				if (document.getElementById("phoneNo").value == ""){
					alert('Please Enter phone number');
					document.getElementById('phoneNo').style.borderColor = "red";
					return false;
				}
				else
				{
					document.getElementById('phoneNo').style.borderColor = "green";
					if (document.getElementById("candbio").value == "") {
						alert('Please Enter biograghy');
						document.getElementById('candbio').style.borderColor = "red";
						return false;
					}
					else
					{
						document.getElementById('candbio').style.borderColor = "green";
						if (document.getElementById("officeAddress").value == "") {
							alert('Please Enter office Address');
							document.getElementById('officeAddress').style.borderColor = "red";
							return false;
						}
						else
						{
							document.getElementById('officeAddress').style.borderColor = "green";
							if (document.getElementById("postcode").value == "") {
								alert('Please Enter post code');
								document.getElementById('postcode').style.borderColor = "red";
								return false;
							}
							else
							{
								document.getElementById('postcode').style.borderColor = "green";
								if (document.getElementById("townCity").value == "") {
									alert('Please Enter Town or City');
									document.getElementById('townCity').style.borderColor = "red";
									return false;
								}
								else
								{
									document.getElementById('townCity').style.borderColor = "green";
									if (document.getElementById("note").value == "") {
										alert('Please Enter message to supporters');
										document.getElementById('note').style.borderColor = "red";
										return false;
									}
									else
									{
										document.getElementById('note').style.borderColor = "green";
										if (document.getElementById("country").value == "") {
											alert('Please pick a country');
											document.getElementById('country').style.borderColor = "red";
											return false;
										}
										else
										{
											document.getElementById('country').style.borderColor = "green";
											if (document.getElementById("donationRange").value == "") {
												alert('Please enter amount funds needed');
												document.getElementById('donationRange').style.borderColor = "red";
												return false;
											}
											else
											{
												document.getElementById('donationRange').style.borderColor = "green";
												if (document.getElementById("officeName").value == "") {
													alert('Please select office to run for');
													document.getElementById('officeName').style.borderColor = "red";
													return false;
												}
												else
												{
													document.getElementById('officeName').style.borderColor = "green";
													if (document.getElementById("lng").value == "") {
														alert('Drag map marker to your location');
														document.getElementById('lng').style.borderColor = "red";
														return false;
													}
													else
													{
														document.getElementById('lng').style.borderColor = "green";
														if (document.getElementById("lat").value == "") {
															alert('Drag map marker to your location');
															document.getElementById('lat').style.borderColor = "red";
															return false;
														}
														else
														{
															document.getElementById('lat').style.borderColor = "green";
															if (document.getElementById("long").value == "") {
																alert('Drag map marker to your location');
																document.getElementById('long').style.borderColor = "red";
																return false;
															}
															else
															{
																document.getElementById('long').style.borderColor = "green";
																if (document.getElementById("latit").value == "") {
																	alert('Drag map marker to your location');
																	document.getElementById('latit').style.borderColor = "red";
																	return false;
																}
																else
																{
																	document.getElementById('latit').style.borderColor = "green";
																}
															}

														}
														return true;
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
}
