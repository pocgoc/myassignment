function validate(input) {
	if (document.getElementById("firstName").value == "") {
		alert('Please Enter First Name');
		document.getElementById('firstName').style.borderColor = "red";
		return false;
	}
	else
	{
		document.getElementById('firstName').style.borderColor = "green";
		if (document.getElementById("lastName").value == "") {
			alert('Please Enter Last Name');
			document.getElementById('lastName').style.borderColor = "red";
			return false;
		}
		else
		{
			document.getElementById('lastName').style.borderColor = "green";
			if (document.getElementById("addressLine1").value == "") {
				alert('Please Enter Address Line 1');
				document.getElementById('addressLine1').style.borderColor = "red";
				return false;
			}
			else
			{
				document.getElementById('addressLine1').style.borderColor = "green";
				if (document.getElementById("addressLine2").value == ""){
					alert('Please Enter Address Line 2');
					document.getElementById('addressLine2').style.borderColor = "red";
					return false;
				}
				else
				{
					document.getElementById('addressLine2').style.borderColor = "green";
					if (document.getElementById("postcode").value == "") {
						alert('Please Enter Post Code');
						document.getElementById('postcode').style.borderColor = "red";
						return false;
					}
					else
					{
						document.getElementById('postcode').style.borderColor = "green";
						if (document.getElementById("townCity").value == "") {
							alert('Please Enter Town or City');
							document.getElementById('townCity').style.borderColor = "red";
							return false;
						}
						else
						{
							document.getElementById('townCity').style.borderColor = "green";
							if (document.getElementById("country").value == "") {
								alert('Please Enter Country');
								document.getElementById('country').style.borderColor = "red";
								return false;
							}
							else
							{
								document.getElementById('country').style.borderColor = "green";
								if (document.getElementById("age").value == "") {
									alert('Please Enter Age');
									document.getElementById('age').style.borderColor = "red";
									return false;
								}
								else
								{
									document.getElementById('age').style.borderColor = "green";
									if (document.getElementById("email").value == "") {
										alert('Please Enter Email Address');
										document.getElementById('email').style.borderColor = "red";
										return false;
									}
									else
									{
										document.getElementById('email').style.borderColor = "green";
										if (document.getElementById("password").value == "") {
											alert('Please Enter password');
											document.getElementById('password').style.borderColor = "red";
											return false;
										}
										else
										{
											document.getElementById('password').style.borderColor = "green";
											if (document.getElementById("candidateCheck").value == "") {
												alert('Please Select Candidate to support');
												document.getElementById('candidateCheck').style.borderColor = "red";
												return false;
											}
											else
											{
												document.getElementById('candidateCheck').style.borderColor = "green";
											}
										}
										
									}
									return true;
								}
							}
						}
					}
				}
			}
		}
	}
}